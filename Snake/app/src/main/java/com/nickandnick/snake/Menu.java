package com.nickandnick.snake;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class Menu extends AppCompatActivity {

    DatabaseHelper mydb;
    Variables var = new Variables();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        mydb = DatabaseHelper.getInstance(this);

        if(var.getViewDialog() == true) {
            final EditText name = new EditText(this);
            name.setHint("name");
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setMessage("Enter a name").setView(name);
            alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                    if(name.getText().toString().matches("")) {
                        return;
                    }

                    mydb.insertData(name.getText().toString(), var.getScore());
                }
            });

            alert.create().show();  //if new score is in top 5
            var.setViewDialog(false);
        }
    }

    public void toPlay(View view) {
        Intent i = new Intent(this, Play.class);
        startActivity(i);
    }

    public void toSettings(View view) {
        Intent i = new Intent(this, Settings.class);
        startActivity(i);
    }

    public void toHighscores(View view) {
        Intent i = new Intent(this, HighScores.class);
        startActivity(i);
    }
}
