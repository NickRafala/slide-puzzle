package com.nickandnick.snake;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by njraf_000 on 4/11/2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    public final static String DATABASE_NAME = "snake.db";
    public final static String TABLE_NAME = "highscores";
    public final static String COL1 = "NAME";
    public final static String COL2 = "SCORE";
    private final int SIZE = 5; //database size
    private Integer[] scores = new Integer[SIZE];
    private String[] names = new String[SIZE];
    private Integer highestScore;
    private static DatabaseHelper mydb = null;

    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    public static DatabaseHelper getInstance(Context context) {
        if(mydb == null)
            mydb = new DatabaseHelper(context);

        return mydb;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + " (" + COL1 + " TEXT, " + COL2 + " INTEGER)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void deleteData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void insertData(String name, int score) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COL1, name);
        values.put(COL2, score);


        Cursor res = getData();
        if(res.getCount() < SIZE) {
            db.insert(TABLE_NAME, null, values);

        }
        else {  //database is full
            int x = 0;

            //read data into arrays
            while(res.moveToNext()) {
                names[x] = res.getString(0);
                scores[x] = new Integer(Integer.parseInt(res.getString(1)));
                x++;    //ends up being equal to SIZE
            }

            //compare new score with score array
            highestScore = new Integer(score);
            for(int a = 0; a < SIZE; a++) {
                if(highestScore.compareTo(scores[a]) < 0) {
                    highestScore = scores[a];
                    x = a;  //save index of lowest value; x == SIZE if new score is lowest
                }
            }


            if(x != SIZE) {
                //delete/overwrite lowest score
                names[x] = name;
                scores[x] = score;


                //sort arrays
                bubbleSort(names, scores);

                //delete database entries
                deleteData();

                //insert arrays as data
                for (int i = 0; i < SIZE; i++) {
                    values.put(COL1, names[i]);
                    values.put(COL2, scores[i]);
                    db.insert(TABLE_NAME, null, values);
                }
            }

        }

        db.close();

    }

    public Cursor getData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_NAME, null);
        return res;
    }

    public void bubbleSort(String names[], Integer scores[]) {
        boolean swapped = true;
        int j = 0;
        Integer tmp;
        String stemp;
        while (swapped) {
            swapped = false;
            j++;
            for (int i = 0; i < scores.length-1 ; i++) {
                if (scores[i].compareTo(scores[i + 1]) > 0) {
                    tmp = scores[i];
                    stemp = names[i];

                    scores[i] = scores[i + 1];
                    names[i] = names[i+1];

                    scores[i + 1] = tmp;
                    names[i + 1] = stemp;
                    swapped = true;
                }
            }
        }
    }
}
