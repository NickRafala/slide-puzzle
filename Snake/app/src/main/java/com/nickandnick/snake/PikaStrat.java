package com.nickandnick.snake;

import android.app.Activity;
import android.widget.ImageView;

/**
 * Created by njraf_000 on 4/26/2016.
 */
public class PikaStrat implements ImageStrat {

    Activity activity;
    int puzzSize = 0;
    ImageView imgView;
    int[][] board;

    public PikaStrat(Activity a, int p, int[][] b) {
        activity = a;
        puzzSize = p;
        board = b;
    }

    @Override
    public void setImage() {
        String name;    //view id
        int id;
        int counter = 1;    //put this value in grid as double array is parsed; 1 to 16
//*
        for(int x = 0; x < puzzSize; x++) {
            for (int y = 0; y < puzzSize; y++) {
                //get view
                name = "v_" + x + "_" + y;
                id = activity.getResources().getIdentifier(name, "id", activity.getPackageName());
                imgView = (ImageView) activity.findViewById(id);

                //set image
                counter = board[x][y];
                name = "pika"+counter;    //+puzzType;
                id = activity.getResources().getIdentifier(name, "drawable", activity.getPackageName());  //convert string name into int id
                imgView.setImageResource(id);
            }
        }
        //*/

    }

    @Override
    public int getHint() {
        int hint = activity.getResources().getIdentifier("pika", "drawable", activity.getPackageName());
        return hint;
    }
}
