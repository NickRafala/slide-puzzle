package com.nickandnick.snake;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class Settings extends AppCompatActivity {

    TextView music;
    TextView sound;
    TextView currentSelect;
    TextView currentColor;
    Variables var;
    String s;
    SharedPreferences pref;
    SharedPreferences.Editor ed;
    Play p;
    int tview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        var = new Variables();
        p = new Play(var);

        pref = getSharedPreferences("keyword", 0);
        ed = pref.edit();
        tview = pref.getInt("color", 1);

        switch(tview) {
            case 1:
                currentSelect = (TextView) findViewById(R.id.strat1);
                break;
            case 2:
                currentSelect = (TextView) findViewById(R.id.strat2);
                break;
            case 3:
                currentSelect = (TextView) findViewById(R.id.strat3);
                break;
            case 4:
                currentSelect = (TextView) findViewById(R.id.strat4);
                break;

        }

        music = (TextView) findViewById(R.id.musicop);
        sound = (TextView) findViewById(R.id.soundsop);
        music.setText(pref.getString("music", "ON").toString());
        sound.setText(pref.getString("sound", "ON").toString());

        currentSelect.setTextColor(Color.parseColor("#ff0000"));
        currentSelect.setBackgroundColor(Color.parseColor("#000000"));

        music.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (music.getText().toString() == "ON") {
                    music.setText("OFF");
                    var.setBgmON(false);
                    var.notifyObs();
                } else {
                    music.setText("ON");
                    var.setBgmON(true);
                    var.notifyObs();
                }
            }
        });

        sound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sound.getText().toString() == "ON") {
                    sound.setText("OFF");
                    var.setSoundsON(false);
                    var.notifyObs();
                } else {
                    sound.setText("ON");
                    var.setSoundsON(true);
                    var.notifyObs();
                }
            }
        });

    }

    public void changeSelect(View v) {

        //change currentSelect to normal colors
        currentSelect.setTextColor(Color.parseColor("#ffffff"));
        currentSelect.setBackgroundColor(Color.parseColor("#000000"));

        currentSelect = (TextView) v;

        //change currentSelect to select colors
        currentSelect.setTextColor(Color.parseColor("#ff0000"));
        currentSelect.setBackgroundColor(Color.parseColor("#000000"));

    }

    public void strat1(View v) {
        var.setStrat(1);
        changeSelect(v);
        var.notifyObs();
        tview = 1;
    }

    public void strat2(View v) {
        var.setStrat(2);
        changeSelect(v);
        var.notifyObs();
        tview = 2;
    }

    public void strat3(View v) {
        var.setStrat(3);
        changeSelect(v);
        var.notifyObs();
        tview = 3;
    }

    public void strat4(View v) {
        var.setStrat(4);
        changeSelect(v);
        var.notifyObs();
        tview = 4;
    }

    @Override
    protected void onPause() {
        super.onPause();

        s = music.getText().toString();
        ed.putString("music", s);
        s = sound.getText().toString();
        ed.putString("sound", s);

        ed.putInt("color", tview);

        ed.commit();
    }
}
