package com.nickandnick.snake;

import java.util.Observable;

/**
 * Created by njraf_000 on 4/17/2016.
 */
public class Variables extends Observable {

    static boolean bgmON = true;
    static boolean sounds = true;
    static char puzzType = 'a';
    static boolean viewDialog = false;
    static int playerScore = 0;
    static int imgStrt;

    public Variables() {

    }

    public void setBgmON(boolean bgm) {
        bgmON = bgm;
    }

    public boolean getBgmON() {
        return bgmON;
    }

    public void setSoundsON(boolean snd) {
        sounds = snd;
    }

    public boolean getSoundsON() {
        return sounds;
    }

    public void setPuzzType(char pt) {
        puzzType = pt;
    }

    public char getPuzzType() {
        return puzzType;
    }

    public void setViewDialog(boolean v) {
        viewDialog = v;
    }

    public boolean getViewDialog() {
        return viewDialog;
    }

    public void setScore(int s) {
        playerScore = s;
    }

    public int getScore() {
        return playerScore;
    }

    public void setStrat(int is) {
        imgStrt = is;
    }

    public int getStrat() {
        return imgStrt;
    }

    public void notifyObs() {
        setChanged();
        notifyObservers();
    }
}
