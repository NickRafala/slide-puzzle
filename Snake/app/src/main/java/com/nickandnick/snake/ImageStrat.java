package com.nickandnick.snake;

/**
 * Created by njraf_000 on 4/26/2016.
 */
public interface ImageStrat {
    void setImage();
    int getHint();
}
