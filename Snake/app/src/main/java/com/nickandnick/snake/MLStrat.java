package com.nickandnick.snake;

import android.app.Activity;
import android.widget.ImageView;

/**
 * Created by njraf_000 on 4/27/2016.
 */
public class MLStrat implements ImageStrat {

    Activity activity;
    int puzzSize = 0;
    ImageView imgView;
    int[][] board;

    public MLStrat(Activity a, int p, int[][] b) {
        activity = a;
        puzzSize = p;
        board = b;
    }

    @Override
    public void setImage() {
        String name;    //view id
        int id;
        int counter = 1;    //put this value in grid as double array is parsed; 1 to 16
//*
        for(int x = 0; x < puzzSize; x++) {
            for (int y = 0; y < puzzSize; y++) {
                //get view
                name = "v_" + x + "_" + y;
                id = activity.getResources().getIdentifier(name, "id", activity.getPackageName());
                imgView = (ImageView) activity.findViewById(id);

                //set image
                counter = board[x][y];
                name = "monalisa"+counter;    //+puzzType;
                id = activity.getResources().getIdentifier(name, "drawable", activity.getPackageName());  //convert string name into int id
                imgView.setImageResource(id);
            }
        }
        //*/

    }

    public int getHint() {
        int hint = activity.getResources().getIdentifier("monalisa", "drawable", activity.getPackageName());
        return hint;
    }
}
