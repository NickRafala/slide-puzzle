package com.nickandnick.snake;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class HighScores extends AppCompatActivity {

    DatabaseHelper mydb = DatabaseHelper.getInstance(this);
    TextView scores;
    Button del;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high_scores);
        scores = (TextView) findViewById(R.id.scores);
        del = (Button) findViewById(R.id.button);

        //mydb.deleteData();

        //sort database content


        viewData();
    }

    public void viewData(){
        Cursor res = mydb.getData();

        if(res.getCount() == 0) {
            scores.setText("Database is empty");
            return;
        }

        StringBuffer buffer = new StringBuffer();
        while(res.moveToNext()) {

            buffer.append("Name: "+res.getString(0)+"\n");
            buffer.append("Score: "+res.getString(1)+"\n\n");
        }



        //paste data on screen
        scores.setText(buffer.toString());
    }

    public void delete(View view) {
        mydb.deleteData();
        viewData();
    }


}
