package com.nickandnick.snake;

import android.app.AlertDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Observable;
import java.util.Observer;
import java.util.Random;

public class Play extends AppCompatActivity implements GestureDetector.OnGestureListener, Observer{

    MediaPlayer swoosh;
    MediaPlayer bgm;
    Observable obs;
    final int puzzSize = 4;
    int temp;
    int moveCount = 0;  //number of moves made to complete puzzle
    final int BLANK = 100;
    int evenOdd = (puzzSize*puzzSize)%2;    // 0=even; 1=odd puzzle size
    Random rand;
    int[][] board = {{1, 2, 3, 4},
            {5, 6, 7, 8},
            {9, 10, 11, 12},
            {13, 14, 15, BLANK}};
    int[] nums = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, BLANK};
    int stratNum = 1;
    GestureDetectorCompat gestuerDetector;
    ImageView imgView;
    TextView count;
    TextView hint;
    Variables var = new Variables();
    boolean bgmON = true;
    boolean soundsON = true;
    ImageStrat myStrat = new NumStrat(this, puzzSize, board);
    char puzzType = 'a';

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        gestuerDetector = new GestureDetectorCompat(this, this);
        count = (TextView) findViewById(R.id.counter);
        swoosh = MediaPlayer.create(this, R.raw.swoosh);
        bgm = MediaPlayer.create(this, R.raw.ganymede);
        hint = (TextView) findViewById(R.id.hint);
        final TextView timer = (TextView) findViewById(R.id.time);

        puzzType = var.getPuzzType();
        bgmON = var.getBgmON();
        soundsON = var.getSoundsON();
        stratNum = var.getStrat();

        switch(stratNum) {
            case 1:
                myStrat = new NumStrat(this, puzzSize, board);
                break;
            case 2:
                myStrat = new SmileyStrat(this, puzzSize, board);
                break;
            case 3:
                myStrat = new MLStrat(this, puzzSize, board);
                break;
            case 4:
                myStrat = new PikaStrat(this, puzzSize, board);
                break;
        }

        if(bgmON == true) {
            bgm.start();
            bgm.setVolume(0.9f, 1.0f);
        }

        init();

        myStrat.setImage();




    }

    public Play() {}

    public Play(Observable o) {
        obs = o;
        obs.addObserver(this);

    }

    public void init() {
        rand = new Random();
        boolean solvable = false;

        do {

            for (int i = 0; i < nums.length; ++i)    //shuffle 1d array
            {
                int j = rand.nextInt(nums.length - i);
                int tmp = nums[nums.length - 1 - i];
                nums[nums.length - 1 - i] = nums[j];
                nums[j] = tmp;
            }

            temp = 0;
            for (int x = 0; x < puzzSize; x++) {    //put 1d array in 2d array
                for (int y = 0; y < puzzSize; y++) {
                    board[x][y] = nums[temp];
                    temp++;
                }
            }

            if(evenOdd == 0) {
                if(evenSolvabilityCheck() == true)
                    solvable = true;
            } else {
                if(oddSolvabilityCheck() == true)
                    solvable = true;
            }

        } while(solvable == false);


    }

    public boolean evenSolvabilityCheck() {
        int inversion = 0;
        boolean evenRow = true;

        //check number of inversions
        for(int x = 0; x < puzzSize*puzzSize-1; x++) {
            for(int y = x+1; y < puzzSize*puzzSize; y++) {
                if(nums[x] == BLANK)
                    break;
                if(nums[y] == BLANK)
                    continue;
                if(nums[x] > nums[y])
                    inversion++;
            }
        }

        //check blank position
        for(int y = puzzSize-1; y > 0; y-=2) {
            for(int x = 0; x < puzzSize; x++) {
                if(board[y][x] == BLANK) {
                    evenRow = false;
                }
            }
        }

        for(int y = puzzSize-2; y >= 0; y-=2) {
            for(int x = 0; x < puzzSize; x++) {
                if(board[y][x] == BLANK) {
                    evenRow = true;
                }
            }
        }


        if((evenRow == true) && (inversion%2 == 1))
            return true;
        else if((evenRow == false) && (inversion%2 == 0))
            return true;
        else
            return false;
    }

    public boolean oddSolvabilityCheck() {
        int inversion = 0;

        //check number of inversions
        for(int x = 0; x < puzzSize*puzzSize-1; x++) {
            for(int y = x+1; y < puzzSize*puzzSize; y++) {
                if(nums[x] == BLANK)
                    break;
                if(nums[y] == BLANK)
                    continue;
                if(nums[x] > nums[y])
                    inversion++;
            }
        }

        if( (inversion%2 == 0) )
            return true;
        else
            return false;
    }



    public void checkWin() {
        int check = 0;

        for(int x = 0; x < puzzSize; x++) {
            for (int y = 0; y < puzzSize; y++) {
                check++;
                if(board[x][y] != check) {
                    if((x == puzzSize-1 && y == puzzSize-1) && board[puzzSize-1][puzzSize-1] != BLANK)
                        return;
                    else if(board[puzzSize-1][puzzSize-1] == BLANK && (x == puzzSize-1 && y == puzzSize-1)) {
                        //you win dialog
                        var.setViewDialog(true);
                        var.setScore(moveCount);

                        Intent i = new Intent(getApplication(), Menu.class);
                        startActivity(i);

                    } else
                        return;
                }

            }
        }

    }



    @Override
    public boolean onTouchEvent(MotionEvent event) {
        gestuerDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

        switch (getSlope(e1.getX(), e1.getY(), e2.getX(), e2.getY())) {
            case 1:
                //up
                for(int x = 1; x < puzzSize; x++) {
                    for (int y = 0; y < puzzSize; y++) {
                        if (board[x - 1][y] == BLANK) {
                            board[x - 1][y] = board[x][y];
                            board[x][y] = BLANK;
                            moveCount++;
                            count.setText(Integer.toString(moveCount));
                            if(soundsON == true) {
                                swoosh.seekTo(50);
                                swoosh.start();
                            }
                            myStrat.setImage();
                            checkWin();
                            return true;
                        }
                    }
                }
                return true;
            case 2:
                //left
                for(int x = 0; x < puzzSize; x++) {
                    for (int y = 1; y < puzzSize; y++) {
                        if (board[x][y-1] == BLANK) {
                            board[x][y-1] = board[x][y];
                            board[x][y] = BLANK;
                            moveCount++;
                            count.setText(Integer.toString(moveCount));
                            if(soundsON == true) {
                                swoosh.seekTo(50);
                                swoosh.start();
                            }
                            myStrat.setImage();
                            checkWin();
                            return true;
                        }
                    }
                }
                return true;
            case 3:
                //down
                for(int x = 0; x < puzzSize-1; x++) {
                    for (int y = 0; y < puzzSize; y++) {
                        if (board[x + 1][y] == BLANK) {
                            board[x + 1][y] = board[x][y];
                            board[x][y] = BLANK;
                            moveCount++;
                            count.setText(Integer.toString(moveCount));
                            if(soundsON == true) {
                                swoosh.seekTo(50);
                                swoosh.start();
                            }
                            myStrat.setImage();
                            checkWin();
                            return true;
                        }
                    }
                }
                return true;
            case 4:
                //right
                for(int x = 0; x < puzzSize; x++) {
                    for (int y = 0; y < puzzSize-1; y++) {
                        if (board[x][y+1] == BLANK) {
                            board[x][y+1] = board[x][y];
                            board[x][y] = BLANK;
                            moveCount++;
                            count.setText(Integer.toString(moveCount));
                            if(soundsON == true) {
                                swoosh.seekTo(50);
                                swoosh.start();
                            }
                            myStrat.setImage();
                            checkWin();
                            return true;
                        }
                    }
                }
                return true;
        }
        return false;
    }

    private int getSlope(float x1, float y1, float x2, float y2) {
        Double angle = Math.toDegrees(Math.atan2(y1 - y2, x2 - x1));
        if (angle > 45 && angle <= 135)
            // up
            return 1;
        if (angle >= 135 && angle < 180 || angle < -135 && angle > -180)
            // left
            return 2;
        if (angle < -45 && angle>= -135)
            // down
            return 3;
        if (angle > -45 && angle <= 45)
            // right
            return 4;
        return 0;
    }

    public void showHint(View v) {
        ImageView imgHint = new ImageView(this);
        imgHint.setImageResource(myStrat.getHint());
        //build dialog
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        //setView(hint)
        alert.setView(imgHint);

        alert.create().show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        bgm.release();
    }

    @Override
    public void update(Observable o, Object data) {
        puzzType = ((Variables)o).getPuzzType();
        bgmON = ((Variables)o).getBgmON();
        soundsON = ((Variables)o).getSoundsON();
        stratNum = ((Variables)o).getStrat();

        switch(stratNum) {
            case 1:
                myStrat = new NumStrat(this, puzzSize, board);
                break;
            case 2:
                myStrat = new SmileyStrat(this, puzzSize, board);
                break;
            case 3:
                myStrat = new SmileyStrat(this, puzzSize, board);
                break;
            case 4:
                myStrat = new NumStrat(this, puzzSize, board);
                break;
        }

    }

}
